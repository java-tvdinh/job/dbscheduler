package com.tvdinh.demo.controller;

import com.tvdinh.demo.service.DemoService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class TestController {

    private final DemoService demoService;

    public TestController(DemoService demoService) {
        this.demoService = demoService;
    }

    @GetMapping("/create-job")
    private ResponseEntity<Void> createJob() {
        demoService.testCreateService();
        return ResponseEntity.ok().build();
    }

}
