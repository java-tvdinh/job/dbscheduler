package com.tvdinh.demo.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NotificationRequestDto {

    private String target;
    private String title;
    private String body;
}