package com.tvdinh.demo.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class TaskData implements Serializable {
    private String id;
}
