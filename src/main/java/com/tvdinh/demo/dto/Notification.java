package com.tvdinh.demo.dto;

import lombok.Data;

@Data
public class Notification {
    private Long id;
    private Boolean isSend;
    private Long eventId;
}
