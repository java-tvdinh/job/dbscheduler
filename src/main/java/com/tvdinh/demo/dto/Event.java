package com.tvdinh.demo.dto;

import lombok.Data;

@Data
public class Event {
    private Long id;
    private String content;
    private String title;
    private Boolean isSend;
}
