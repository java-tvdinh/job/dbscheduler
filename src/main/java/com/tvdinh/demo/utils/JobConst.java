package com.tvdinh.demo.utils;

public interface JobConst {
    String NAME_TASK = "demo-task";
    String NAME_TASK_DATA = "demo-task-data";

    Integer TIME_RE_RUN_TASK_DEAD = 15; // (realtime = TIME_RE_RUN_TASK_DEAD * 4)
    Integer MAX_RETRIES_TASK = 3;
    Integer RETRY_DELAY = 3;
    String CRON_POLICY_DELETE_OLD_LOG_TASK_DATA = "policy-delete-old-log-cron";
}
