package com.tvdinh.demo.service.impl;

import com.github.kagkarlsson.scheduler.Scheduler;
import com.github.kagkarlsson.scheduler.task.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import com.tvdinh.demo.dto.Event;
import com.tvdinh.demo.dto.NotificationRequestDto;
import com.tvdinh.demo.dto.TaskData;
import com.tvdinh.demo.model.EventEntity;
import com.tvdinh.demo.model.NotificationEntity;
import com.tvdinh.demo.repository.EventRepository;
import com.tvdinh.demo.repository.NotificationRepository;
import com.tvdinh.demo.service.DemoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@Service
@Slf4j
public class DemoServiceImpl implements DemoService {

    private final Task<String> demoTask;
    private final Task<TaskData> taskData;
    private final Scheduler scheduler;
    private final EventRepository eventRepository;
    private final NotificationRepository notificationRepository;

    public DemoServiceImpl(Task<String> demoTask, Task<TaskData> taskData, Scheduler scheduler, EventRepository eventRepository, NotificationRepository notificationRepository) {
        this.demoTask = demoTask;
        this.taskData = taskData;
        this.scheduler = scheduler;
        this.eventRepository = eventRepository;
        this.notificationRepository = notificationRepository;
    }

    @Transactional
    @Override
    public void testRunTaskService(String id) {
        log.info("Start run task. Data id: {}", id);
        List<EventEntity> entityList = eventRepository.findAll();
        if (!CollectionUtils.isEmpty(entityList)) {
            Event event = new Event();
            EventEntity e = entityList.get(0);
            event.setId(e.getId());
            event.setContent(e.getContent());
            event.setTitle(e.getTitle());
            event.setIsSend(true);
            //save event
            saveEvent(event);
//            if (Objects.nonNull(event)) {
//                throw new RuntimeException("error");
//            }
            String targetToken ="dHpzBQmHIJhHYVnfaBF6xO:APA91bEONSt0r3rhU1R4Dq2cfJ6ix9YvKepxyOLTlQJlHLLoNwwbXw-8ut-N4eUwo_dgKHdbcy1QuuDkOYNgbUILaBjygywkBeZcqx4sFtJ2PpWtuXvGfIinpmkc-ca9QZqq81u2_gqz";
            //String targetToken = "3rtr";
            sendPnsToDevice(NotificationRequestDto.builder()
                    .target(targetToken)
                    .body("nội dung").title("hello").build());
            //save notification
            com.tvdinh.demo.dto.Notification notification = new com.tvdinh.demo.dto.Notification();
            notification.setEventId(e.getId());
            notification.setIsSend(true);
            saveNotification(notification);

            // Test create job in transaction
            //testCreateService();
            testCreateTaskData();

            //call api asnyc
//            if (Objects.nonNull(notification)) {
//                throw new RuntimeException("error");
//            }
//            RestTemplate restTemplate = new RestTemplate();
//            ResponseEntity<String> res = restTemplate.getForEntity("http://locahost:3000", String.class);
//            if (HttpStatus.OK.equals(res.getStatusCode())) {
//                log.info("Call api success: {}", res.getBody());
//            } else {
//                log.info("call api false");
//            }
        }
    }

    @Override
    @Transactional
    public void testCreateService() {
        String id = UUID.randomUUID().toString();
        log.info("Data id: {}", id);
        scheduler.schedule(demoTask.instance(UUID.randomUUID().toString(), id), Instant.now());
    }

    @Transactional
    public void testCreateTaskData() {
        String id = UUID.randomUUID().toString();
        log.info("Task Data id: {}", id);
        scheduler.schedule(taskData.instance(UUID.randomUUID().toString(), TaskData.builder().id(id).build()), Instant.now());
    }


    @Override
    public void testRecurring() {
        log.info("Task recurring...");
    }

    public String sendPnsToDevice(NotificationRequestDto notificationRequestDto) {
        Message message = Message.builder()
                .setToken(notificationRequestDto.getTarget())
                .setNotification(Notification.builder().setTitle(notificationRequestDto.getTitle()).setBody(notificationRequestDto.getBody()).build())
                .putData("content", notificationRequestDto.getTitle())
                .putData("body", notificationRequestDto.getBody())
                .build();

        String response = null;
        try {
            log.info("message: {}", message);
            response = FirebaseMessaging.getInstance().sendAsync(message).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        return response;
    }

    @Transactional()
    public void saveEvent(Event e) {
        EventEntity eventEntity = new EventEntity();
        eventEntity.setId(e.getId());
        eventEntity.setContent(e.getContent());
        eventEntity.setTitle(e.getTitle());
        eventEntity.setIsSend(e.getIsSend());
        eventRepository.save(eventEntity);
    }

    @Transactional()
    public void saveNotification(com.tvdinh.demo.dto.Notification notification) {
        NotificationEntity notificationEntity = new NotificationEntity();
        notificationEntity.setId(notification.getId());
        notificationEntity.setIsSend(notification.getIsSend());
        notificationEntity.setEventId(notification.getEventId());
        notificationRepository.save(notificationEntity);
    }
}
