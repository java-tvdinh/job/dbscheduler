package com.tvdinh.demo.service;

public interface DemoService {

    void testRunTaskService(String id);

    void testCreateService();

    void testRecurring();

}
