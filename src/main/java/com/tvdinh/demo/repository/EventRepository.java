package com.tvdinh.demo.repository;

import com.tvdinh.demo.model.EventEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventRepository extends JpaRepository<EventEntity, Long> {
}
