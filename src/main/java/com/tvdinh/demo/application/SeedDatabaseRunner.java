package com.tvdinh.demo.application;

import com.github.kagkarlsson.scheduler.Scheduler;
import com.github.kagkarlsson.scheduler.task.helper.PlainScheduleAndData;
import com.github.kagkarlsson.scheduler.task.helper.RecurringTaskWithPersistentSchedule;
import com.github.kagkarlsson.scheduler.task.schedule.Schedules;
import com.tvdinh.demo.dto.PolicyDeleteOldLogDTO;
import com.tvdinh.demo.model.EventEntity;
import com.tvdinh.demo.repository.EventRepository;
import com.tvdinh.demo.service.DemoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.time.Duration;
import java.util.List;
import java.util.Objects;

@Component
@Slf4j
public class SeedDatabaseRunner implements CommandLineRunner {


    private final DemoService demoService;
    private final EventRepository eventRepository;
    private final Scheduler scheduler;
    private final RecurringTaskWithPersistentSchedule<PlainScheduleAndData> cronPolicyDeleteOldLog;

    public SeedDatabaseRunner(@Lazy DemoService demoService, EventRepository eventRepository, Scheduler scheduler,
                              RecurringTaskWithPersistentSchedule<PlainScheduleAndData> cronPolicyDeleteOldLog) {
        this.demoService = demoService;
        this.eventRepository = eventRepository;
        this.scheduler = scheduler;
        this.cronPolicyDeleteOldLog = cronPolicyDeleteOldLog;
    }

    @Override
    public void run(String... args) {
        testDeleteTask();
        List<EventEntity> entityList = eventRepository.findAll();
        if (CollectionUtils.isEmpty(entityList)) {
            EventEntity e = new EventEntity();
            e.setTitle("Demo");
            e.setContent("Content");
            e.setIsSend(false);
            eventRepository.save(e);
        }
        for (int i = 0; i < 1; i++) {
            demoService.testCreateService();
        }
    }

    private void testDeleteTask() {
        scheduler.schedule(cronPolicyDeleteOldLog.schedulableInstance("id2", new PlainScheduleAndData(Schedules.fixedDelay(Duration.ofSeconds(6)),
                PolicyDeleteOldLogDTO.builder().tenantId("test").build())));

        scheduler.schedule(cronPolicyDeleteOldLog.schedulableInstance("id3", new PlainScheduleAndData(Schedules.parseSchedule("FIXED_DELAY|23s"),
                PolicyDeleteOldLogDTO.builder().tenantId("test").build())));

        scheduler.schedule(cronPolicyDeleteOldLog.schedulableInstance("id4", new PlainScheduleAndData(Schedules.cron(buildCronEveryMinute(1)),
                PolicyDeleteOldLogDTO.builder().tenantId("test").build())));

//        try {
//            scheduler.cancel(cronPolicyDeleteOldLog.instanceId("id2"));
//        } catch (Exception e) {
//            log.error("Task instance remove fail: ");
//        }
    }

    private String buildCronEveryMinute(Integer minute) {
        return String.format("0 */%d * ? * *", minute);
    }

    private String buildCronDay(Integer minute, Integer hour, Integer day) {
        if (Objects.isNull(day)) {
            return String.format("0 %d %d ?" +
                    " * *", minute);
        } else {
            return String.format("0 %d %d %d * *", minute, hour, day);
        }
    }

}
