package com.tvdinh.demo.config;

import com.github.kagkarlsson.scheduler.SchedulerName;
import com.github.kagkarlsson.scheduler.boot.config.DbSchedulerCustomizer;
import com.github.kagkarlsson.scheduler.task.DeadExecutionHandler;
import com.github.kagkarlsson.scheduler.task.Task;
import com.github.kagkarlsson.scheduler.task.helper.PlainScheduleAndData;
import com.github.kagkarlsson.scheduler.task.helper.RecurringTaskWithPersistentSchedule;
import com.github.kagkarlsson.scheduler.task.helper.Tasks;
import com.tvdinh.demo.dto.PolicyDeleteOldLogDTO;
import com.tvdinh.demo.dto.TaskData;
import com.tvdinh.demo.service.DemoService;
import com.tvdinh.demo.utils.JobConst;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.util.Optional;
import java.util.UUID;

import static com.github.kagkarlsson.scheduler.task.schedule.Schedules.cron;

@Configuration
@AutoConfigureAfter({DemoService.class})
@Slf4j
public class TaskConfiguration {

    private static final String SYNC_TASK = "sync-task";

    @Value("${app.job.sync-status.cron}")
    private String cronExpression;

    @Autowired
    private @Lazy DemoService demoService;

    @Bean
    Task<String> demoTask() {
        return Tasks.oneTime(JobConst.NAME_TASK, String.class)
                .onDeadExecution(new DeadExecutionHandler.ReviveDeadExecution<>())   // re-run task which was dead when shutdown server
                .execute(((taskInstance, executionContext) -> {
                    log.info("Start execution with task name: " + taskInstance.getTaskName()
                            + " with task instance: " + taskInstance.getTaskAndInstance());
                    String ticketId = taskInstance.getData();
                    demoService.testRunTaskService(ticketId);
                }));
    }

//    @Bean
//    Task<String> demoTaskRetry() {
//        return Tasks.oneTime(JobConst.NAME_TASK, String.class)
//                .onFailure(new FailureHandler.MaxRetriesFailureHandler<>(JobConst.MAX_RETRIES_TASK, (executionComplete, executionOperations) -> {
//                    // try again in 3 second
//                    log.info("Execution has failed " + executionComplete.getExecution().consecutiveFailures + " times. Trying again in a bit...");
//                    executionOperations.reschedule(executionComplete, Instant.now().plusSeconds(JobConst.RETRY_DELAY));
//                }))
//                .onDeadExecution(new DeadExecutionHandler.ReviveDeadExecution<>())   // re-run task which was dead when shutdown server
//                .execute(((taskInstance, executionContext) -> {
//                    log.info("Start execution with task name: " + taskInstance.getTaskName()
//                            + " with task instance: " + taskInstance.getTaskAndInstance());
//                    String dataId = taskInstance.getData();
//                    demoService.testRunTaskService(dataId);
//                }));
//    }


    @Bean
    Task<TaskData> demoTaskData() {
        return Tasks.oneTime(JobConst.NAME_TASK_DATA, TaskData.class)
                .onDeadExecution(new DeadExecutionHandler.ReviveDeadExecution<>())   // re-run task which was dead when shutdown server
                .execute(((taskInstance, executionContext) -> {
                    log.info("Start execution with task name: " + taskInstance.getTaskName()
                            + " with task instance: " + taskInstance.getTaskAndInstance());
                    TaskData taskData = taskInstance.getData();
                    log.info("taskData: {}", taskData);
                }));
    }

    @Bean
    Task<Void> syncTask() {
        return Tasks
                .recurring(SYNC_TASK, cron(cronExpression))
                .execute((taskInstance, executionContext) -> {
                    log.info("Start execution recurring with task name: " + taskInstance.getTaskName()
                            + " with task instance: " + taskInstance.getTaskAndInstance());
                    demoService.testRecurring();
                });
    }

    @Bean
    RecurringTaskWithPersistentSchedule<PlainScheduleAndData> cronPolicyDeleteOldLog() {
        return Tasks.recurringWithPersistentSchedule(JobConst.CRON_POLICY_DELETE_OLD_LOG_TASK_DATA, PlainScheduleAndData.class)
                .execute((taskInstance, executionContext) -> {
                    log.info("Start execution recurring with task name: " + taskInstance.getTaskName()
                            + " with task instance: " + taskInstance.getTaskAndInstance());
                    PolicyDeleteOldLogDTO dto = (PolicyDeleteOldLogDTO) taskInstance.getData().getData();
                });
    }

    @Bean
    DbSchedulerCustomizer customizer() {
        return new DbSchedulerCustomizer() {
            @Override
            public Optional<SchedulerName> schedulerName() {
                return Optional.of(new SchedulerName.Fixed(String.format("%s-%s", "scheduler", UUID.randomUUID())));
            }
        };
    }

}
