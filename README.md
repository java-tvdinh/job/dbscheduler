# DbScheduler
```Để sử dụng kiểu dữ liệu object cho data của task 
Ex: Task<TaskData>... {}
Do data của task lưu với kiểu byte --> Do đó class TaskData cần implements Serializable của lớp java.io.Serializable
```
```
[?????]
Tạo @Bean(Tasks.oneTime) chỉ được tạo 1 loại data ví dụ Task<String> demoTask thì ko thể tạo 1 task khác cũng kiểu string
nhưng có thể tạo 1 task khác ví dụ Task<Data> demoTask.
==> Tạo đc n bean Tasks.oneTime. với kiểu dữ liệu khác nhau
[Đúng]
1.
    @Bean
    @Qualifier("sendMailTask")
    Task<String> sendMailTask() {return Tasks.oneTime(JobConst.SEND_MAIL_TASK, String.class)}
2. 
    @Qualifier("sendUserSchedulerTask")
    Task<String> sendUserSchedulerTask() {return Tasks.oneTime(JobConst.SEND_USER_SCHEDULER_TASK, String.class)}

Khi sử dụng 
private final Task<String> sendMailTask;
private final Task<String> sendUserSchedulerTask;
contructor(
    @Qualifier("sendMailTask") Task<String> sendMailTask,
    @Qualifier("sendUserSchedulerTask") Task<String> sendUserSchedulerTask) {

    }
Nếu không để  Qualifier thì khi tạo private final Task<String> sendMailTask nó sẽ lấy theo Qualifier mặc định trong contructor
contructor(
    Task<String> sendMailTask,
    Task<String> sendUserSchedulerTask) {
    }
```
